# Django API

An easy interface to write APIs for the django projects.

## Version
1.0

## Requirements
Django ver 1.8+

## HOWTO

Add the following code into **urls.py**

```python
from django.conf.urls import include, url
from djangoapi import api

api.provider.discover()

urlpatterns += [
  url(r'^api/', include(api.provider.urls)),
]
```

Create a file by name **api.py** in each of the INSTALLED_APPS, where the APIs needs to be defined.

```python
from djangoapi import api

@api.provider.register(
    version='1.0',
    namespace='function_namespace',
    api_name='api_name',
    http_methods=['GET', 'POST']
)
def api_function(request):
    # API code here
    ...
```
